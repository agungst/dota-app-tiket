//
//  Endpoints.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation

private protocol EndpointType {
    var url: String { get }
}

private struct Api {
    static let baseUrl = "https://api.opendota.com"
    static let slug = "api"
}


enum Endpoint {
    
    enum App: EndpointType {
        
        case hero
        case imageUrl

        public var url: String {
            switch self {
            case .hero: return "\(Api.baseUrl)/\(Api.slug)/herostats"
            case .imageUrl: return "\(Api.baseUrl)"
            }
        }
        
    }

}
