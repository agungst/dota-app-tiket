//
//  HeroRequest.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyUserDefaults

protocol HeroRequestDelegate: class {
    func success(_ object: [Hero])
    func failed()
}

class HeroRequest: ApiInterface {
    var method: HTTPMethod?
    
    var url: URLConvertible
    
    var headers: HTTPHeaders?
    
    var parameters: Parameters?
    
    var encoding: ParameterEncoding?
    
    var requestHandler: HeroRequestDelegate?
    
    
    // MARK: Inits
    required init() {
        method = .get
        url = Endpoint.App.hero.url
    }
    
    func start() {
        ApiHelper.apiRequest(api: self)
    }
    
    func prepare(requestHandler: HeroRequestDelegate) {
        self.requestHandler = requestHandler
    }
        
    func success(_ value: JSON) {
        createObject(value)
    }
    
    func failed(_ value: JSON?) {
        requestHandler?.failed()
    }
    
    func createObject(_ value: Any) {
        guard let json = value as? JSON else { return }
        if let arr = json.array {
            var heroList: [Hero] = []
            
            arr.forEach { (hero) in
                if let dict = hero.dictionaryObject, let obj = Hero(JSON: dict) {
                    heroList.append(obj)
                }
            }
            
            requestHandler?.success(heroList)
        } else {
            failed(nil)
        }

    }
}
