//
//  CommonHelper.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import Alamofire
import SwifterSwift

class CommonHelper {
    
    // MARK: - Common Functions
    static let shared = CommonHelper()
    private init() {} //This prevents others from using the default '()' initializer for this class.
    
    func isConnectedToInternet() -> Bool {
        let networkReachabilityManager = NetworkReachabilityManager()
        return networkReachabilityManager?.isReachable ?? false
    }
    
    func testFunction() {
        // Isi Function
    }
    
    func buildHeroListViewController() -> HeroListViperViewController {
        let view = HeroListViperViewController(nibName: String(describing: HeroListViperViewController.self), bundle: nil)
        let interactor = HeroListViperInteractor()
        let router = HeroListViperRouter()
        let presenter = HeroListViperPresenter(interface: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.output = presenter
        router.viewController = view
        return view
    }
    
    func buildHeroDetailViewController(hero: Hero) -> HeroDetailViperViewController {
        let view = HeroDetailViperViewController(nibName: String(describing: HeroDetailViperViewController.self), bundle: nil)
        let interactor = HeroDetailViperInteractor()
        let router = HeroDetailViperRouter()
        let presenter = HeroDetailViperPresenter(interface: view, interactor: interactor, router: router)
        presenter.hero = hero
        view.presenter = presenter
        interactor.output = presenter
        router.viewController = view
        return view
    }
    
}
