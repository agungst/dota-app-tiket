//
//  Extensions.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyUserDefaults

#if canImport(FLEX) && !Release
import FLEX
import AudioToolbox
#endif

extension UIWindow {
    
    #if canImport(FLEX) && !Release
    open override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
            FLEXManager.shared.showExplorer()
        }
    }
    #endif
    
}

extension UIViewController {
    static func loadFromNib() -> Self {
        func instantiateFromNib<T: UIViewController>() -> T {
            return T.init(nibName: String(describing: T.self), bundle: nil)
        }
        
        return instantiateFromNib()
    }
    
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
        
}

extension UICollectionViewCell {
    static func register(for collectionView: UICollectionView)  {
        let cellName = String(describing: self)
        let cellIdentifier = cellName
        let cellNib = UINib(nibName: String(describing: self), bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: cellIdentifier)
    }
}

extension DefaultsKeys {
    var hero: DefaultsKey<[Hero]?> { .init("hero") }
    var roles: DefaultsKey<[String]?> { .init("roles") }
}
