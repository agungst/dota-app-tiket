//
//  HeroListFilterCollectionViewCell.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit

class HeroListFilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var label: UILabel!
    
    var role: String? {
        didSet {
            label.text = role
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
