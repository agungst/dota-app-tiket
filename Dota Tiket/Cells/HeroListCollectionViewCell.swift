//
//  HeroListCollectionViewCell.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 28/10/20.
//  Copyright © 2020 Agung Setiawan. All rights reserved.
//

import UIKit
import Kingfisher

class HeroListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var hero: Hero? {
        didSet {
            guard let hero = hero else { return }
            
            label.text = hero.name
            
            guard let url = URL(string: "\(Endpoint.App.imageUrl.url)\(hero.img)") else { return }
            
            imageView.kf.setImage(with: url)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.kf.indicatorType = .activity
    }

}
