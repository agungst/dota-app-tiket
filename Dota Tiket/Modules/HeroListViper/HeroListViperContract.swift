//
//  HeroListViperContract.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

protocol HeroListViperRouterProtocol: class {
    func goToDetail(hero: Hero)
}

protocol HeroListViperPresenterProtocol: class {
    
    var entity: [Hero]? { get set }
    var filteredEntity: [Hero]? { get set }
    var roles: [String]? { get set }

    func viewDidLoad()
    func filterEntity(role: String)
    func goToDetail(hero: Hero)

}

protocol HeroListViperInteractorOutputProtocol: class {
    
    func requestDidSuccess(object: [Hero])
    func requestDidFailed(message: String)
}

protocol HeroListViperInteractorInputProtocol: class {
    
    var output: HeroListViperInteractorOutputProtocol? { get set }

    func fetchData()
    
}

protocol HeroListViperViewProtocol: class {
    
    // Presenter -> ViewController
    func populateData()
    func showProgressDialog(show: Bool)
    func showAlertMessage(title: String, message: String)
    func showErrorMessage(message: String)
    
}
