//
//  HeroListViperInteractor.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import Foundation

class HeroListViperInteractor: HeroListViperInteractorInputProtocol {
    
    weak var output: HeroListViperInteractorOutputProtocol?
    
    let request = HeroRequest()
    
    func fetchData() {
        request.prepare(requestHandler: self)
        request.start()
    }
    
}

extension HeroListViperInteractor: HeroRequestDelegate {
    
    func success(_ object: [Hero]) {
        output?.requestDidSuccess(object: object)
    }
    
    func failed() {
        output?.requestDidFailed(message: "Failed to fetch data")
    }
    
}
