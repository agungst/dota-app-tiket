//
//  HeroListViperViewController.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import PKHUD

class HeroListViperViewController: UIViewController {
    
    @IBOutlet weak var filterColView: UICollectionView!
    @IBOutlet weak var heroColView: UICollectionView!

    var presenter: HeroListViperPresenterProtocol?
        
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        presenter?.viewDidLoad()
    }

    // MARK: - publics
    // Add public functions here

    // MARK: - IBActions
    // Add IBActions in this area

    // MARK: - Privates
    private func setupViews() {
        // TODO: Add setting up views here
        HeroListFilterCollectionViewCell.register(for: filterColView)
        HeroListCollectionViewCell.register(for: heroColView)
        
        title = "All Heroes"
    }
}

// MARK: - View Protocol
extension HeroListViperViewController: HeroListViperViewProtocol {
    func populateData() {
        // TODO: Populate data
        filterColView.reloadData()
        heroColView.reloadData()
    }

    func showProgressDialog(show: Bool) {
        DispatchQueue.main.async {
//            show ? HUD.show(.progress) : HUD.hide()
        }
    }

    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.async {
            self.showAlert(title: title, message: message)
        }
    }

    func showErrorMessage(message: String) {
        DispatchQueue.main.async {
            self.showAlert(title: nil, message: message)
        }
    }
}

extension HeroListViperViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == filterColView {
            return presenter?.roles?.count ?? 0
        } else {
            if let fh = presenter?.filteredEntity {
                return fh.count
            } else {
                return presenter?.entity?.count ?? 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == filterColView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HeroListFilterCollectionViewCell.self), for: indexPath) as! HeroListFilterCollectionViewCell
            cell.role = presenter?.roles?[safe: indexPath.item]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HeroListCollectionViewCell.self), for: indexPath) as! HeroListCollectionViewCell
            if let fh = presenter?.filteredEntity {
                cell.hero = fh[safe: indexPath.item]
            } else {
                cell.hero = presenter?.entity?[safe: indexPath.item]
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == filterColView {
            return CGSize(width: 120, height: 40)
        } else {
            return CGSize(width: (collectionView.frame.size.width - 16) / 3, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == filterColView {
            guard let selectedRole = presenter?.roles?[safe: indexPath.item] else { return }
            title = selectedRole
            
            presenter?.filterEntity(role: selectedRole)
            
        } else {
            if let fh = presenter?.filteredEntity, let hero = fh[safe: indexPath.item] {
                presenter?.goToDetail(hero: hero)
            } else if let hero = presenter?.entity?[safe: indexPath.item] {
                presenter?.goToDetail(hero: hero)
            }
        }
    }
    
}

