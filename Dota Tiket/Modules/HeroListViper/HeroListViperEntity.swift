//
//  HeroListViperEntity.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyUserDefaults

class Hero: Mappable, Codable, DefaultsSerializable {
    
    var id = 0
    var name = ""
    var icon = ""
    var img = ""
    var attackType = ""
    var primaryAttr = ""
    var roles: [String] = []
    var attackMinVal = 0
    var attackMaxVal = 0
    var defVal = 0
    var moveVal = 0
    var hpVal = 0
    var manaVal = 0

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["localized_name"]
        icon <- map["icon"]
        img <- map["img"]
        attackType <- map["attack_type"]
        primaryAttr <- map["primary_attr"]
        roles <- map["roles"]
        attackMinVal <- map["base_attack_min"]
        attackMaxVal <- map["base_attack_max"]
        defVal <- map["base_str"]
        moveVal <- map["base_agi"]
        hpVal <- map["base_health"]
        manaVal <- map["base_mana"]
    }
    
}
