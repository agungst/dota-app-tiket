//
//  HeroListViperRouter.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

class HeroListViperRouter: HeroListViperRouterProtocol {
    
    weak var viewController: HeroListViperViewController?

     // MARK: RouterProtocol
    func goToDetail(hero: Hero) {
        let vc = CommonHelper.shared.buildHeroDetailViewController(hero: hero)
        viewController?.navigationController?.pushViewController(vc)
    }

}
