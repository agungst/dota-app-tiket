//
//  HeroListViperPresenter.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class HeroListViperPresenter: HeroListViperPresenterProtocol {
    
    weak private var view: HeroListViperViewProtocol?
    private let interactor: HeroListViperInteractorInputProtocol?
    private let router: HeroListViperRouterProtocol?

    var entity: [Hero]?
    var filteredEntity: [Hero]?
    var roles: [String]?
    
    init(interface: HeroListViperViewProtocol, interactor: HeroListViperInteractorInputProtocol?, router: HeroListViperRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    // MARK: PresenterProtocol

    func viewDidLoad() {
        // TODO: Add on view loaded
        view?.showProgressDialog(show: true)
        interactor?.fetchData()
        entity = Defaults[\.hero]
        setupRoles()
    }
    
    func filterEntity(role: String) {
        var filteredHero: [Hero] = []
        
        entity?.forEach({ (hero) in
            if hero.roles.contains(role) {
                filteredHero.append(hero)
            }
        })
        filteredEntity = filteredHero
        
        view?.populateData()
    }
    
    func goToDetail(hero: Hero) {
        router?.goToDetail(hero: hero)
    }
        
    func setupRoles() {
        
        guard let entity = entity else { return }
        
        var roles = Set<String>()
        entity.forEach { (hero) in
            hero.roles.forEach { (role) in
                roles.insert(role)
            }
        }
        self.roles = Array(roles)
    }

}

extension HeroListViperPresenter: HeroListViperInteractorOutputProtocol {
    
    // MARK: InteractorOutputProtocol

    func requestDidSuccess(object: [Hero]) {
        entity = object
        Defaults[\.hero] = object

        setupRoles()
        
        view?.showProgressDialog(show: false)
        view?.populateData()
    }

    func requestDidFailed(message: String) {
        view?.showProgressDialog(show: false)
        view?.showErrorMessage(message: message)
    }

}
