//
//  HeroDetailViperViewController.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import UIKit

class HeroDetailViperViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var attackLabel: UILabel!
    @IBOutlet weak var defLabel: UILabel!
    @IBOutlet weak var movLabel: UILabel!
    @IBOutlet weak var hpLabel: UILabel!
    @IBOutlet weak var manaLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var similarStackView: UIStackView!
    @IBOutlet weak var similarColView: UICollectionView!
    
    var presenter: HeroDetailViperPresenterProtocol?
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        presenter?.viewDidLoad()
    }

    // MARK: - publics
    // Add public functions here

    // MARK: - IBActions
    // Add IBActions in this area

    // MARK: - Privates
    private func setupViews() {
        guard let hero = presenter?.hero else { return }
        
        HeroListCollectionViewCell.register(for: similarColView)

        if let url = URL(string: "\(Endpoint.App.imageUrl.url)\(hero.img)") {
            imageView.kf.setImage(with: url)
        }
        title = hero.name
        nameLabel.text = hero.name
        roleLabel.text = hero.roles.joined(separator: ",")
        
        attackLabel.text = "Attack: \(hero.attackMinVal) - \(hero.attackMaxVal)\n(\(hero.attackType))"
        defLabel.text = "Strength: \(hero.defVal)"
        movLabel.text = "Move Speed: \(hero.moveVal)"
        hpLabel.text = "HP: \(hero.hpVal)"
        manaLabel.text = "MP: \(hero.manaVal)"
        typeLabel.text = "Type: \(hero.primaryAttr)"
    }
}

// MARK: - View Protocol
extension HeroDetailViperViewController: HeroDetailViperViewProtocol {
    func populateData() {
        similarColView.reloadData()
    }

    func showProgressDialog(show: Bool) {
        DispatchQueue.main.async {
            
        }
    }

    func showAlertMessage(title: String, message: String) {
        DispatchQueue.main.async {
            
        }
    }

    func showErrorMessage(message: String) {
        DispatchQueue.main.async {
            
        }
    }
}

extension HeroDetailViperViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: HeroListCollectionViewCell.self), for: indexPath) as! HeroListCollectionViewCell
        cell.hero = presenter?.similarHero?[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width - 16) / 3, height: collectionView.frame.size.height)
    }
    
}
