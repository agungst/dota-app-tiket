//
//  HeroDetailViperContract.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

protocol HeroDetailViperRouterProtocol: class {

}

protocol HeroDetailViperPresenterProtocol: class {
    
    var entity: [Hero]? { get set }
    var hero: Hero! { get set }
    var similarHero: [Hero]? { get set }
    
    func viewDidLoad()

}

protocol HeroDetailViperInteractorOutputProtocol: class {
    
    func requestDidSuccess(object: [Hero])
    func requestDidFailed(message: String)
}

protocol HeroDetailViperInteractorInputProtocol: class {
    
    var output: HeroDetailViperInteractorOutputProtocol? { get set }

    func fetchData()
    
}

protocol HeroDetailViperViewProtocol: class {
    
    // Presenter -> ViewController
    func populateData()
    func showProgressDialog(show: Bool)
    func showAlertMessage(title: String, message: String)
    func showErrorMessage(message: String)
    
}
