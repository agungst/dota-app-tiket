//
//  HeroDetailViperPresenter.swift
//  Dota Tiket
//
//  Created by Agung Setiawan on 05/04/21.
//  Copyright © 2021 Agung Setiawan. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class HeroDetailViperPresenter: HeroDetailViperPresenterProtocol {
    
    weak private var view: HeroDetailViperViewProtocol?
    private let interactor: HeroDetailViperInteractorInputProtocol?
    private let router: HeroDetailViperRouterProtocol?

    var entity: [Hero]?
    var hero: Hero!
    var similarHero: [Hero]?

    init(interface: HeroDetailViperViewProtocol, interactor: HeroDetailViperInteractorInputProtocol?, router: HeroDetailViperRouterProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    // MARK: PresenterProtocol

    func viewDidLoad() {
        // TODO: Add on view loaded
        view?.showProgressDialog(show: true)
        interactor?.fetchData()
        entity = Defaults[\.hero]
        setupSimilarHero(hero: hero)
        view?.populateData()
    }
    
    func setupSimilarHero(hero: Hero) {
        
        var similarHero: [Hero]?

        switch hero.primaryAttr {
        case "agi":
            similarHero = entity?.sorted(by: { (a, b) -> Bool in
                return a.moveVal > b.moveVal
            })
        case "str":
            similarHero = entity?.sorted(by: { (a, b) -> Bool in
                return a.attackMaxVal > b.attackMaxVal
            })
        case "int":
            similarHero = entity?.sorted(by: { (a, b) -> Bool in
                return a.manaVal > b.manaVal
            })
        default:
            similarHero = entity
        }
        
        guard let sh = similarHero?.filter({ $0.id != hero.id }) else { return }
        
        self.similarHero = Array(sh.prefix(3))
        
    }

}

extension HeroDetailViperPresenter: HeroDetailViperInteractorOutputProtocol {
    
    // MARK: InteractorOutputProtocol

    func requestDidSuccess(object: [Hero]) {
        view?.showProgressDialog(show: false)
        view?.populateData()
    }

    func requestDidFailed(message: String) {
        view?.showProgressDialog(show: false)
        view?.showErrorMessage(message: message)
    }

}
